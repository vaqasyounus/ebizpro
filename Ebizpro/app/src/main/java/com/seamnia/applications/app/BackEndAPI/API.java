package com.seamnia.applications.app.BackEndAPI;

import com.google.gson.JsonElement;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;


public interface API {

    //static int pageNumber = 0;
    @GET("/webservice/get_data?from=20980")
    void GetAlldata(Callback<JsonElement> callback);

//link?page_no2&from=4445

    @Multipart
    @POST("/webservice/get_data")
    void GetdataID(@Part("from") Integer record_Id, Callback<JsonElement> callback);





    /*
        @GET("/mage_store2/webservice.php/")
        void GetAllCategories(Callback<JsonElement> callback);
    */
//
    @Multipart
    @POST("/gcm/web_add")
    void getRigisterOnServer(@Part("reg_id") String reg_Id, @Part("device_id") String device_Id,
                             @Part("mobile") String mobile, @Part("model_no")
                                     String model_no, Callback<JsonElement> callback);


//    @Multipart
//    @POST("/seamnia/Webservice/device_register")
//    void RegisterDevice(@Part("user_email") String user_email, @Part("user_password") String user_password, @Part("reg_id") String reg_id, @Part("device_id") String device_id,@Part("mobile") String mobile,@Part("model_no") String model_no, Callback<JsonElement> callback);


    @Multipart
    @POST("/Webservice/user_register")
    void Registeruser(@Part("full_name") String full_name,
                      @Part("client_id") String client_id,
                      @Part("mobile_number") String mobile_number,
                      @Part("address") String address,
                      @Part("refrence_id") String refrence_id,
                      @Part("user_email") String user_email,
                      @Part("user_password") String user_password,
                      Callback<JsonElement> callback);


    @Multipart
    @POST("/Webservice/device_register")
    void LoginUpdateUser(@Part("user_email") String user_email,
                         @Part("user_password") String user_password,
                         @Part("client_id") String client_id,
                         @Part("reg_id") String reg_id,
                         @Part("device_id") String device_id,
                         @Part("model_no") String model_no,
                         @Part("address") String address,
                         @Part("mobile") String mobile,
                         Callback<JsonElement> callback);




    @Multipart
    @POST("/webservice/get_url")
    void geturl_latestGroup(@Part("client") String client_id, @Part("email") String user_email, @Part("type")String limit, @Part("limit") String type ,Callback<JsonElement> callback);



    @Multipart
    @POST("/webservice/get_url")
    void geturl_latest(@Part("client") String client_id, @Part("email") String user_email, @Part("type") String type ,Callback<JsonElement> callback);



    @Multipart
    @POST("/webservice/get_url")
    void geturl(@Part("client") String user_name, @Part("email") String client_id, @Part("type") String limit,@Part("limit") String type, Callback<JsonElement> callback);

    @Multipart
    @POST("/webservice/get_url")
    void geturlThirdparty(@Part("client") String user_name, @Part("email") String client_id, @Part("type")  String type, Callback<JsonElement> callback);



    @Multipart
    @POST("/Webservice/change_pass")
    void ChangePassword(@Part("email") String user_email,
                         @Part("old_password") String user_old_password,
                         @Part("new_password") String user_new_password,
                         Callback<JsonElement> callback);



    @Multipart
    @POST("/webservice/view_multi_client/")
    void getallUserList(@Part("email") String user_name, @Part("password") String password, Callback<JsonElement> callback);




    @Multipart
    @POST("/webservice/add_multi_client/")
    void add_client(@Part("email") String user_name, @Part("password") String password,
                    @Part("client_id") String client_id,
                    Callback<JsonElement> callback);



    @Multipart
    @POST("/webservice/delete_multi_client/")
    void delete_client(@Part("email") String user_name, @Part("password") String password,
                    @Part("client_id") String client_id,
                    Callback<JsonElement> callback);





//    @Multipart
//    @POST("/seamnia/Webservice/user_register")
//    void RegisterUser(@Part("user_email") String user_email, @Part("user_password") String user_password, @Part("full_name") String full_name,  Callback<JsonElement> callback);


//    @Multipart
//    @POST("/roomaif/index.php?route=product/webservices/getallcat")
//    void checkhit(@Part("security_token") String security_token, Callback<JsonElement> callback);

}






