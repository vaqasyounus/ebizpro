package com.seamnia.applications.app.BackEndAPI;


import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

import static retrofit.RestAdapter.Builder;

public class RestClient {
    private static API REST_CLIENT;

    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static API get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(5, TimeUnit.MINUTES);
        okHttpClient.setReadTimeout(5, TimeUnit.MINUTES);

        Builder builder = new Builder()
                .setEndpoint(Utils.AdminPageURL)
                .setClient(new OkClient(okHttpClient)).setLogLevel(RestAdapter.LogLevel.FULL);
        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(API.class);

    }
}