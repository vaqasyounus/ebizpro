package com.seamnia.applications.app;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonElement;
import com.seamnia.applications.app.BackEndAPI.RestClient;
import com.seamnia.applications.app.app.Config;
import com.seamnia.applications.app.util.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ChangePasswordActivity extends AppCompatActivity {


    ProgressDialog progressDialog;
    Button btnback;

    EditText input_email_change, old_password, new_password,new_password_confirm;
    Button btn_done;
     String oldPassword,newPassword,emailChange , newPassword_retype;
    boolean issucess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        input_email_change = (EditText) findViewById(R.id.input_email_change);
        old_password = (EditText) findViewById(R.id.input_old_password);
        new_password = (EditText) findViewById(R.id.input_password_new);
        btn_done = (Button) findViewById(R.id.btn_done);
        btnback = (Button)findViewById(R.id.btnback);
        new_password_confirm = (EditText)findViewById(R.id.input_password_new_retype);


        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                emailChange = input_email_change.getText().toString();
                 oldPassword = old_password.getText().toString();
                 newPassword = new_password.getText().toString();
                newPassword_retype = new_password_confirm.getText().toString();


                if ( emailChange.equals("")) {
                    input_email_change.setError("Please insert your email");
                }

                else if (!isEmailValid(emailChange)) {
                    input_email_change.setError("Please insert correct email");

                } else if (oldPassword.equals("")) {
                    old_password.setError("Please enter old password");

                } else if (newPassword.equals("")) {
                    new_password.setError("Please enter new password");
                } else if (newPassword.length() < 6) {
                    new_password.setError("password is too short ");
                }
                else if (newPassword.length() < 6) {
                    new_password.setError("password is too short ");
                }



               else if (!(newPassword_retype.equals(newPassword)))
                {

                    new_password_confirm.setError("New Password not matched");
                }
                else {
                    if (!(internetConnectionAvailable(500))) {
                        Toast.makeText(ChangePasswordActivity.this, "No internet Connected..!!", Toast.LENGTH_SHORT).show();

                    } else {
                        changePassword();
                    }
                }
            }
        });


        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }


    private void changePassword() {
        progressDialog = new ProgressDialog(ChangePasswordActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        RestClient.get().ChangePassword(emailChange, oldPassword, newPassword, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());

                    String status = jsonObject.getString("status");
                    String webresponce = jsonObject.getString("msg");

                    switch (status) {
                        case "0": //login id doesNaot Matched
                            issucess =false;
                            ShoDialogBox(getApplicationContext(),webresponce);
                            break;
                        case "1": //Password Successfully Changed

                            if(webresponce.equals("Password Successfully Changed"))
                            {
                                issucess =true;
                                ShoDialogBox(getApplicationContext(),"Password Successfully Changed");
                            }

                            break;
                        case "-2":

                            if(webresponce.contains("Invalid Username"))
                            {
                                issucess = false;
                                ShoDialogBox(getApplicationContext(),"Invalid Username/password");
                            }
                            break;
                        default:
                    }

                } catch (JSONException e) {
                    issucess =false;
                    progressDialog.dismiss();
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

                Log.v("ExceptionResult", error.toString());
            }
        });
    }
    public void ShoDialogBox(final Context context, String message) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                ChangePasswordActivity.this).create();
        // Setting Dialog Title
        alertDialog.setTitle("Notification");
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                if(issucess)
                {
                    finish();
                }
            }
        });
        // Showing Alert Message
        try {
            alertDialog.show();
        } catch (Exception e) {
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    private boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        } catch (TimeoutException e) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }

}



