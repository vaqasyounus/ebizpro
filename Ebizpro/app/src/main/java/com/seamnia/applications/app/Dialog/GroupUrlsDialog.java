package com.seamnia.applications.app.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.seamnia.applications.app.R;
import com.seamnia.applications.app.adpater.GroupUrl_adapterView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GroupUrlsDialog extends Dialog {
    Context context;
    private String url;
    String CName;

    RelativeLayout bg;
    ImageButton btn_addWord,btn_cross;
    private RecyclerView recycler_view;
//    private List<String> listUrls ;
    List<String> listUrls ,listtime,listCid;
    int id;
    public GroupUrlsDialog(Context context, List urlsList,List listtime,List listCid ) {
        super(context);
        this.context = context;
        this. listUrls = urlsList;
        this.listtime = listtime;
        this.listCid = listCid;
        this.CName = CName;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_goupurls);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
       // getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        init();
    }
    public void init() {

        btn_cross = (ImageButton)findViewById(R.id.btn_cross);
        recycler_view = (RecyclerView) findViewById(R.id.recyclerView_swapText);
        recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(layoutManager);
       // Collections.reverse(listUrls);
        GroupUrl_adapterView swap_adapter = new GroupUrl_adapterView(context, listUrls,listtime,listCid);
        recycler_view.setAdapter(swap_adapter);
        swap_adapter.notifyDataSetChanged();
        btn_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });


    }
}
