package com.seamnia.applications.app;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonElement;
import com.seamnia.applications.app.BackEndAPI.RestClient;
import com.seamnia.applications.app.BackEndAPI.Utils;
import com.seamnia.applications.app.app.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity {

    public String user_email, user_password;
    EditText inputLayoutEmail, passwordText;
    Button loginBtn;
    ProgressDialog progressDialog;
    private String device_id;
    private String model_no;
    private String mobileType;
    private String reg_id;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    SharedPreferences pref;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    TextView registration_link;
    public static final String MY_PREFS_NAME = "MyPrefs";
    String client_id;
    SharedPreferences settings;
    TextView changePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputLayoutEmail = (EditText) findViewById(R.id.input_email);
        passwordText = (EditText) findViewById(R.id.input_password);
        settings = getApplicationContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        loginBtn = (Button) findViewById(R.id.btn_signlogin);
        registration_link = (TextView) findViewById(R.id.registration_link);
        changePassword = (TextView) findViewById(R.id.change_password);
        DeviceConfig();
        String signup = "Signup";
        String first = "";
        String last = "";
        String next = "<font color='#0000FF'>" + signup + "</font>";
        registration_link.setText(Html.fromHtml(first + " " + next + " " + last));
        changePassword.setText(Html.fromHtml("<font style='bold' color='blue'>" + "Change Password"));
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            client_id = bundle.getString("client_id");
        }
        if (!(internetConnectionAvailable(500))) {
            Toast.makeText(LoginActivity.this, "No internet Connected..!!", Toast.LENGTH_SHORT).show();
        }
        editor = getPreferences(MODE_PRIVATE).edit();
        prefs = getPreferences(MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        reg_id = pref.getString("regId", null);
        if (!(reg_id != null && !reg_id.isEmpty())) {
            loadPushToGCM("from main");
        }
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_email = inputLayoutEmail.getText().toString();
                user_password = passwordText.getText().toString();
                if (user_email.equals("")) {
                    inputLayoutEmail.setError("Please enter email");
                } else if (!isEmailValid(user_email)) {
                    inputLayoutEmail.setError("Please enter valid email");
                } else if (user_password.equals("")) {
                    passwordText.setError("Please enter password");
                } else {
                    if (!(internetConnectionAvailable(500))) {
                        Toast.makeText(LoginActivity.this, "No internet Connected..!!", Toast.LENGTH_SHORT).show();
                        //loginBtn.setBackgroundResource(R.color.abc_primary_text_disable_only_material_dark);
                    } else {
                        if (reg_id != null && !reg_id.isEmpty()) {
                            RegisterDeviceToWebDevice();
                        } else {
                            Toast.makeText(LoginActivity.this, "Unable to register your device for Notifications. Please restart the app.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        registration_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent registrationintent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registrationintent);
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent test = new Intent(LoginActivity.this, ChangePasswordActivity.class);
                startActivity(test);
            }
        });
    }

    private void RegisterDeviceToWebDevice() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        RestClient.get().LoginUpdateUser(user_email, user_password,client_id, reg_id, device_id, model_no, " ", model_no, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    String webresponce = jsonObject.getString("msg");
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    switch (status) {
                        case "0": //login id doesNaot Matched
                            ShoDialogBox(getApplicationContext(), webresponce);
                            break;
                        case "-1": //login id doesNaot Matched
                            ShoDialogBox(getApplicationContext(), webresponce);
                            break;
                        case "1":// login /device updated
                            if (webresponce.equals("Successfully login and  new device added")) {
                                String client_id = jsonObject.getJSONObject("data").getString("cliend_id");
                                String url = jsonObject.getJSONObject("data").getString("url");
                                String adsize = jsonObject.getJSONObject("data").getString("adsize");
                                String aboutTitle = jsonObject.getJSONObject("data").getString("about_title");
                                String advTitle = jsonObject.getJSONObject("data").getString("advert_title");
                                String fourTitle = jsonObject.getJSONObject("data").getString("four_title");
                                String fourUrl = jsonObject.getJSONObject("data").getString("four_url");
                                String logout_falg = jsonObject.getJSONObject("data").getString("logout_falg");
                                if (logout_falg != null) {
                                    if (!logout_falg.equals("1"))
                                        logout_falg = "0";
                                }

                                String color = "#0c6fa6";
                                try {
                                    color = jsonObject.getJSONObject("data").getString("color");
                                } catch (Exception e) {
                                }

                                if (url.equals("0")) {
                                    url = "http://seamnia.net";
                                }
                                settings.edit().clear().commit();
                                editor.putInt("logged", 1);
                                editor.putString("client_id", client_id);
                                editor.putString("clientUrl", url);
                                editor.putString("user_email", user_email);
                                editor.putString("user_password", user_password);
                                editor.putString("adsize", adsize);
                                editor.putString("color", color);
                                editor.putString("about_title", aboutTitle);
                                editor.putString("advert_title", advTitle);
                                editor.putString("four_title", fourTitle);
                                editor.putString("four_url", fourUrl);
                                editor.putString("logout_falg", logout_falg);


                                editor.commit();
                                Log.d("log", jsonObject.getString("msg"));
                                Intent testPass1 = new Intent(getApplicationContext(), MainActivity.class);
                                testPass1.putExtra("user_email", user_email);
                                Utils.isfirstime = true;
                                // testPass1.putExtra("client_id", client_id);
                                startActivity(testPass1);
                                finish();
                            } else {
                                ShoDialogBox(getApplicationContext(), "Please register first");
                            }
                            //ShoDialogBox(getApplicationContext(), "Please register first");
                            break;
                        case "-2":// login unSucessfull
                            if (webresponce.equals("Invalid Username/Password")) {
                                ShoDialogBox(getApplicationContext(), "Invalid user name please signup");
                            }
                            break;
                        default:
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    public void ShoDialogBox(final Context context, String message) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                LoginActivity.this).create();
        // Setting Dialog Title
        alertDialog.setTitle("Notification");
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                NotificationManager notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notifManager.cancelAll();
            }
        });
        // Showing Alert Message
        try {
            alertDialog.show();
        } catch (Exception e) {
        }
    }

    public void DeviceConfig() {
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        model_no = android.os.Build.MODEL;
        mobileType = android.os.Build.MANUFACTURER;
    }

    private boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        } catch (TimeoutException e) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }

    public void loadPushToGCM(final String msg) {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId(msg);
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    private void displayFirebaseRegId(String msg) {
        Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_SHORT).show();
        Log.d("Sucess", "Sucess");
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

}
