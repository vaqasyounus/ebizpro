package com.seamnia.applications.app;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.JsonElement;
import com.seamnia.applications.app.BackEndAPI.RestClient;
import com.seamnia.applications.app.BackEndAPI.Utils;
import com.seamnia.applications.app.Dialog.GroupUrlsDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import im.delight.android.webview.AdvancedWebView;
import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, AdvancedWebView.Listener {
    AdvancedWebView webView_Home;
    AdvancedWebView webView_Refresh;
    WebView webView_third_party;
    View middle_border;
    public static final String MY_PREFS_NAME = "MyPrefs";
    public static final String MY_PREFS_BAGE = "MyPrefs Bage";
    public static final String MY_PREFS_TIME = "MyPrefsTime";
    private boolean isRedirected;
    String messageUrl;
    ProgressDialog progressDialog;
    String user_name;
    String adsize;
    String color;
    String fourUrl;

    String isNotification = "";
    String client_id;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    int isFromSplash;
    Toolbar toolbar;
    ImageButton btnhome;
    private boolean homepage;
    String ClientUrl;
    SharedPreferences prefs;
    SharedPreferences prefsBageCounts;
    SharedPreferences prefsTime;

    GroupUrlsDialog customdialog;
    private boolean ispageThirdPartyloaded;
    private boolean advertisement, aboutus, latestmesage;
    private boolean isFirsttimetest;
    private boolean isFirsttime;
    int count;
    ImageButton btngetnotification;
    private String preferenceTime;
    private String currentTime;
    LinearLayout linerar_layout;
    private boolean thirdpartyclicked;
    private AdView mAdView;


    private ImageButton phone;
    private ImageButton calander;


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    static public boolean isURLReachable(Context context, String strUrl) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            try {

                URL url = new URL("https://www.google.com.pk/");
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setConnectTimeout(10 * 1000);
                urlc.connect();
                if (urlc.getResponseCode() == 200) {
                    return true;
                } else {
                    return false;
                }


            } catch (MalformedURLException e1) {
                return false;
            } catch (IOException e) {
                return false;
            }
        }
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, getString(R.string.ad_app_id));
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please wait...");
        middle_border = (View) findViewById(R.id.middleborder);
        toolbar = (Toolbar) findViewById(R.id.toolbar_header);
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                color = prefs.getString("color", "#0c6fa6");
            }
        }
        try {
            toolbar.setBackgroundColor(Color.parseColor(color));
            middle_border.setBackgroundColor(Color.parseColor(color));
        } catch (Exception e) {
        }

        toolbar.setTitle("");
        toolbar.showOverflowMenu();
        setSupportActionBar(toolbar);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        phone = findViewById(R.id.phone);

        calander = findViewById(R.id.cal);
        calander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(MainActivity.this, com.seamnia.applications.app.WebView.class);
//                i.putExtra("event_weblink", getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("event_weblink", ""));
//                i.putExtra("color",getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("color",""));
//                startActivity(i);

                startActivity(new Intent(MainActivity.this, com.seamnia.applications.app.reminder.MainActivity.class));
            }
        });

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        /*        final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.phone);
                dialog.setTitle("Contact");

                TextView address = (TextView) dialog.findViewById(R.id.addressD);
                TextView phone = (TextView) dialog.findViewById(R.id.phoneD);
                address.setText(getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("client_address", ""));
                phone.setText(getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("mobile_number", ""));

                Button dialogButton = (Button) dialog.findViewById(R.id.call_now);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();*/

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

                alertDialogBuilder.setTitle("Contact");
                alertDialogBuilder.setMessage(getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("client_address", "") + "\n\n" +
                        getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("mobile_number", ""));
                alertDialogBuilder.setPositiveButton("Call Now",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                callPhoneNumber();
                            }
                        });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

        });

        webView_Home = (AdvancedWebView) findViewById(R.id.webView_Home);
        webView_Home.setListener(this, this);
        webView_Refresh = (AdvancedWebView) findViewById(R.id.webView_Refresh);
        webView_Refresh.setListener(this, this);
        webView_third_party = (WebView) findViewById(R.id.webView_third_party);
        btnhome = (ImageButton) findViewById(R.id.btnhome);
        btngetnotification = (ImageButton) findViewById(R.id.btngetnotification);


        webView_third_party.getSettings().setJavaScriptEnabled(true);
        webView_third_party.getSettings().setBuiltInZoomControls(true);
        //  webView_third_party.setWebViewClient(new WebViewClient());
        webView_third_party.getSettings().setSupportZoom(true);
        settings = getApplicationContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        linerar_layout = (LinearLayout) findViewById(R.id.linera_layout);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                user_name = bundle.getString("user_email");
                isFromSplash = bundle.getInt("fromsplash");
                isNotification = bundle.getString("message");

                prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                String restoredText = prefs.getString("text", null);
                client_id = prefs.getString("client_id", "");
                user_name = prefs.getString("user_email", "");
                adsize = prefs.getString("adsize", "30");
                if (adsize.equals("0") || adsize.equals("1")) {
                    webView_third_party.setVisibility(View.GONE);
                }

                if (adsize.equals("1")) {
                    mAdView.setVisibility(View.VISIBLE);
                }
                color = prefs.getString("color", "#0c6fa6");
                ClientUrl = prefs.getString("clientUrl", "http://seamnia.net/");

                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        Toolbar.LayoutParams.MATCH_PARENT,
                        Toolbar.LayoutParams.MATCH_PARENT,
                        1.0f
                );
//adjusting add size
                ScreenAdjustSize(adsize);

                //if (isFromSplash == 1) {
                if ((isURLReachable(MainActivity.this, ""))) {
                    if (isNotification != null) {
                        getRequestUrl();
                    } else {
                        progressDialog.show();
                        startWebView_Refresh(ClientUrl);
                    }
                    getRequestThirdPartUrl();
                    ispageThirdPartyloaded = true;
                } else {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                    alertDialog.setTitle(Html.fromHtml("<font color='#f7941e'>Error</font>"));
                    alertDialog.setMessage("Connection Failure, Tap to retry.");
                    //alertDialog.setIcon(android.R.drawable.ic_dialog_info);
                    alertDialog.setCancelable(false);
                    alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialog.show();
                    return;
                }
            }

            startWebViewHome(ClientUrl);
        }

//        boolean success = ShortcutBadger.removeCount(MainActivity.this);
        prefsBageCounts = getSharedPreferences(MY_PREFS_BAGE, MODE_PRIVATE);
        prefsBageCounts.edit().clear().commit();


        webView_third_party.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                thirdpartyclicked = true;
                // Toast.makeText(MainActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        btnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView_Home.setVisibility(View.VISIBLE);
                webView_Refresh.setVisibility(View.GONE);
                ScreenAdjustSize(adsize);
                if (!ispageThirdPartyloaded) {
                    getRequestThirdPartUrl();
                }
            }
        });

        btngetnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((isURLReachable(MainActivity.this, ""))) {

                    getRequestUrl();
//                    btngetnotification.setBackgroundResource(R.mipmap.ic_launcher);
                }
            }
        });

        getRequestUrlCompareTime();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String adv = prefs.getString("advert_title", "Advertisement");
            String about = prefs.getString("about_title", "About");
            String four = prefs.getString("four_title", "Menu Item");
            fourUrl = prefs.getString("four_url", "Menu Item");
            String logout_falg = prefs.getString("logout_falg", "1");


            if (TextUtils.isEmpty(adv.trim())) {
                menu.getItem(0).setVisible(false);
            } else {
                menu.getItem(0).setTitle(adv);
                menu.getItem(0).setVisible(true);
            }

            if (TextUtils.isEmpty(about.trim())) {
                menu.getItem(1).setVisible(false);
            } else {
                menu.getItem(1).setTitle(about);
                menu.getItem(1).setVisible(true);
            }


            if (TextUtils.isEmpty(four.trim())) {
                menu.getItem(4).setVisible(false);
            } else {
                menu.getItem(4).setTitle(four);
                menu.getItem(4).setVisible(true);
            }
            if (logout_falg.equals("0")) {
                menu.getItem(5).setVisible(false);
            } else {
                menu.getItem(5).setTitle("Logout");
                menu.getItem(5).setVisible(true);
            }


           /* if (four.equals("Menu Item")) {
                menu.getItem(4).setVisible(false);
            } else {
                menu.getItem(4).setTitle(four);

            }*/
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.advertisement:
                advertisement = true;
                webView_Home.setVisibility(View.GONE);
                webView_Refresh.setVisibility(View.VISIBLE);
                getUrl_latest();
                break;
            case R.id.four:
                progressDialog.show();
                webView_Refresh.loadUrl(fourUrl);
                break;
            case R.id.logout_menu:
                logoutDialog();
                break;
            case R.id.about_us:
                aboutus = true;
                webView_Home.setVisibility(View.GONE);
                webView_Refresh.setVisibility(View.VISIBLE);
                get_aboutUrl();
                break;
            case R.id.latest_messages:
                latestmesage = true;
                webView_Home.setVisibility(View.GONE);
                webView_Refresh.setVisibility(View.VISIBLE);
                get_LatestUrlsGroup();
                break;
            case R.id.manage_Clients:
                manage_Clients();
                break;
        }
        return true;
    }

    public void manage_Clients() {
        Intent manageIntent = new Intent(MainActivity.this, ManageClients.class);
        startActivity(manageIntent);
    }

    private void startWebView_Refresh(String url) {

        if (!(this).isFinishing()) {
//            progressDialog.show();
        }


        webView_Refresh.loadUrl(url);
    }


    private void startWebView_Test(String url) {

        if (!(this).isFinishing()) {
//            progressDialog.show();
        }

        webView_third_party.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                isRedirected = true;

                return false;

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                isRedirected = false;
            }

            public void onLoadResource(WebView view, String url) {
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    progressDialog.dismiss();
                } catch (Exception ignored) {
                }
            }
        });
        webView_third_party.loadUrl(url);
    }


    private void startWebView_ThirdParty(String url) {
        if (!(this).isFinishing()) {
            //  progressDialog.show();
        }
        webView_third_party.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //view.loadUrl(url);
                if (count == 2) {
                    count = 0;

                } else {
                    if (thirdpartyclicked) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                        thirdpartyclicked = false;
                        Utils.backfromUrl = true;
                        isRedirected = true;
                        count = 0;
                    }
                }
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                isRedirected = false;
                count++;
            }

            public void onLoadResource(WebView view, String url) {
                Log.d("url", url);
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    isFirsttime = true;
                    progressDialog.dismiss();
                } catch (Exception ignored) {
                }
            }
        });
        // webView_third_party.setWebChromeClient(new WebChromeClient());

        webView_third_party.loadUrl(url);
    }

    private void startWebViewHome(String url) {
        progressDialog.show();
        webView_Home.loadUrl(url);
    }

    private boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (Exception ignored) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            messageUrl = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + messageUrl);
            if (!Utils.isShowed) {
                ShoDialogBox(MainActivity.this, messageUrl, "You have a new message. Please use the refresh option from menu to view the new message.");
                Utils.isShowed = true;
            }
        }
    };

    private void ShoDialogBox(final Context context, final String url, final String message) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                MainActivity.this).create();
        alertDialog.setTitle("Notification");
        alertDialog.setMessage(message);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setCancelable(false);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        try {
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (webView_Home.getVisibility() == View.VISIBLE) {
            outState.putBoolean("HomeVisible", true);
            outState.putString("ClientUrl", ClientUrl);
            webView_Home.saveState(outState);
        } else {
            outState.putBoolean("HomeVisible", false);
            // webView_Refresh.saveState(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean("HomeVisible")) {
                // webView_Home.setVisibility(View.VISIBLE);
                // webView_Refresh.setVisibility(View.GONE);
                ClientUrl = savedInstanceState.getString("ClientUrl");
                webView_Home.restoreState(savedInstanceState);
                // startWebViewHome(ClientUrl);
            } else {
                // webView_Refresh.setVisibility(View.VISIBLE);
                // webView_Home.setVisibility(View.GONE);
            }
        }
    }

    public void logoutDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure logout as " + user_name + " ?");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                settings.edit().clear().commit();
                Intent testpass = new Intent(getApplicationContext(), RegisterActivityNew.class);
                startActivity(testpass);
                dialog.cancel();
                finish();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private void getUrl_latest() {
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        client_id = prefs.getString("client_id", "");
        user_name = prefs.getString("user_email", "");
        //user_name = prefs.getString("user_email", "");
        String type = "2";
        RestClient.get().geturl_latest(client_id, user_name, type, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    String url = jsonObject.getString("msg");
                    String webresponce = jsonObject.getString("msg");
                    JSONObject json2 = jsonObject.getJSONObject("msg");
                    String adcert_url = json2.getString("advert_url");
                    Log.d("status", status + " :" + webresponce);
                    switch (status) {
                        case "-1":
                            editor.putString("webUrl", "http://seamnia.net/");
                            editor.commit();
                            startWebView_Refresh("http://seamnia.net/");
                            break;
                        case "1":
                            editor.putString("webUrl", adcert_url);
                            editor.commit();
                            startWebView_Refresh(adcert_url);
                        default:
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    private void get_aboutUrl() {
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        client_id = prefs.getString("client_id", "");
        user_name = prefs.getString("user_email", "");
        //user_name = prefs.getString("user_email", "");
        String type = "1";
        RestClient.get().geturl_latest(client_id, user_name, type, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    String url = jsonObject.getString("msg");
                    String webresponce = jsonObject.getString("msg");
                    JSONObject json2 = jsonObject.getJSONObject("msg");
                    String about_url = json2.getString("about_url");
                    Log.d("status", status + " :" + webresponce);
                    switch (status) {
                        case "-1":
                            editor.putString("webUrl", "http://seamnia.net/");
                            editor.commit();
                            startWebView_Refresh("http://seamnia.net/");
                            break;
                        case "1":
                            editor.putString("webUrl", about_url);
                            editor.commit();
                            startWebView_Refresh(about_url);
                        default:
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    private void get_LatestUrlsGroup() {
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        client_id = prefs.getString("client_id", "");
        user_name = prefs.getString("user_email", "");
        //user_name = prefs.getString("user_email", "");
        String type = "3";
        String limit = "15";
        RestClient.get().geturl_latestGroup(client_id, user_name, type, limit, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    if (!status.equals("-1")) {
                        List listUrls = new ArrayList();
                        List listTime = new ArrayList();
                        List listCID = new ArrayList();
                        List listTimes = new ArrayList();
                        switch (status) {
                            case "-1":
                                editor.putString("webUrl", "http://seamnia.net/");
                                editor.commit();
                                startWebView_Refresh("http://seamnia.net/");
                                break;
                            case "1":
                                JSONObject jobj = jsonObject.getJSONObject("msg");
                                JSONArray jarrayurl = jobj.getJSONArray("msg");
                                JSONArray jarraytime = jobj.getJSONArray("time");
                                JSONArray jarraycid = jobj.getJSONArray("cid");
                                JSONArray jarraytimes = jobj.getJSONArray("time_ms");
                                for (int i = 0; i < jarrayurl.length(); i++) {
                                    listUrls.add(jarrayurl.get(i));
                                    listTime.add(jarraytime.get(i));
                                    listCID.add(jarraycid.get(i));
                                }
                                listTimes.add(jarraytimes.get(0));
                                prefsTime = getSharedPreferences(MY_PREFS_TIME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_TIME, MODE_PRIVATE).edit();
                                editor.putString("time", listTimes.get(0).toString());
                                editor.commit();
                                customdialog = new GroupUrlsDialog(MainActivity.this, listUrls, listTime, listCID);
                                customdialog.show();
                                // Log.d("done","");
                            default:
                        }
                    } else {
                        //Toast.makeText(MainActivity.this, "-1 Response from server", Toast.LENGTH_SHORT).show();
                        AlrtDialogNOMSg();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    private void getRequestUrl() {
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        client_id = prefs.getString("client_id", "");
        user_name = prefs.getString("user_email", "");
        String type = "3";
        String limit = "1";
        RestClient.get().geturl(client_id, user_name, type, limit, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    String msgresponce = jsonObject.getString("msg");
                    if (!status.equals("-1")) {
                        List listUrl = new ArrayList();
                        List listTime = new ArrayList();
                        JSONObject jobj = jsonObject.getJSONObject("msg");
                        JSONArray jarrayurl = jobj.getJSONArray("msg");
                        JSONArray jarraytime = jobj.getJSONArray("time_ms");
                        listUrl.add(jarrayurl.get(0));
                        listTime.add(jarraytime.get(0));
                        switch (status) {
                            case "-1":
                                editor.putString("webUrl", "http://seamnia.net/");
                                editor.commit();
                                startWebView_Refresh("http://seamnia.net/");
                                break;
                            case "1":
                                webView_Home.setVisibility(View.GONE);
                                webView_Refresh.setVisibility(View.VISIBLE);
                                editor.putString("webUrl", listUrl.get(0).toString());
                                editor.commit();
                                Log.d("test", listTime.get(0).toString());
                                prefsTime = getSharedPreferences(MY_PREFS_TIME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_TIME, MODE_PRIVATE).edit();
                                editor.putString("time", listTime.get(0).toString());
                                editor.commit();
                                startWebView_Refresh(listUrl.get(0).toString());
                            default:
                        }
                    } else {
                        editor.putString("webUrl", "http://seamnia.net/");
                        editor.commit();
                        startWebView_Refresh("http://seamnia.net/");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    private void getRequestUrlCompareTime() {
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        client_id = prefs.getString("client_id", "");
        user_name = prefs.getString("user_email", "");
        String type = "3";
        String limit = "1";
        RestClient.get().geturl(client_id, user_name, type, limit, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    if (!status.equals("-1")) {
                        List listTime = new ArrayList();
                        JSONObject jobj = jsonObject.getJSONObject("msg");
                        JSONArray jarraytime = jobj.getJSONArray("time_ms");
                        listTime.add(jarraytime.get(0));
                        currentTime = jarraytime.get(0).toString();
                        switch (status) {
                            case "-1":
                                break;
                            case "1":
                                prefsTime = getSharedPreferences(MY_PREFS_TIME, MODE_PRIVATE);
                                preferenceTime = prefsTime.getString("time", "0");
                                if (!preferenceTime.equals("0")) {
                                    if (currentTime.compareTo(preferenceTime) > 0) {
//                                        btngetnotification.setBackgroundResource(R.mipmap.update_notification);
                                    }
                                }
                            default:
                        }
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }


    private void getRequestThirdPartUrl() {
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        client_id = prefs.getString("client_id", "");
        user_name = prefs.getString("user_email", "");
        String type = "0";
        RestClient.get().geturlThirdparty(client_id, user_name, type, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    if (!status.equals("-1")) {
                        JSONObject json2 = jsonObject.getJSONObject("msg");
                        String thirdParty_url = json2.getString("third_party_url");
                        if (!thirdParty_url.equals("null")) {
                            switch (status) {
                                case "-1":
                                    editor.putString("webUrl", "http://seamnia.net/");
                                    editor.commit();
                                    startWebView_ThirdParty("http://seamnia.net/");
                                    break;
                                case "1":
                                    editor.putString("webUrl", thirdParty_url);
                                    editor.commit();
                                    startWebView_ThirdParty(thirdParty_url);
                                    //startWebView_Test(thirdParty_url);
                                default:
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Third party Url Address Not Saved on server", Toast.LENGTH_SHORT).show();
                            //  editor.putString("webUrl", "http://seamnia.net/");
                            // editor.commit();
                            startWebView_ThirdParty("http://seamnia.net/");
                        }
                    } else {
                        //Toast.makeText(MainActivity.this, "-1 Response from server", Toast.LENGTH_SHORT).show();
                        AlrtDialogNOMSg();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    public void selectedGroupUrls(String SlectedUrlsName) {
        editor.putString("webUrl", SlectedUrlsName);
        editor.commit();
        startWebView_Refresh(SlectedUrlsName);
        if (customdialog != null) {
            customdialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        if (webView_Refresh.isFocused() && webView_Refresh.canGoBack() || webView_Home.isFocused() && webView_Home.canGoBack()) {
            webView_Refresh.goBack();
            webView_Home.goBack();
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    protected void onResume() {
        boolean success = ShortcutBadger.removeCount(MainActivity.this);

        if (Utils.isfromfirebase) {
            homepage = false;
            //webView_Home.setVisibility(View.GONE);
            //webView_Refresh.setVisibility(View.VISIBLE);
            // getRequestUrl();
            getRequestThirdPartUrl();
            Utils.isfromfirebase = false;
        }
        if (Utils.backfromUrl) {
            getRequestThirdPartUrl();
            count = 0;
            Utils.backfromUrl = false;
        }

        super.onResume();

        mAdView.resume();
        webView_Refresh.onResume();
        webView_Home.onResume();


       /* if (!isNetworkAvailable()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.setTitle(Html.fromHtml("<font color='#f7941e'>Error</font>"));
            alertDialog.setMessage("Connection Failure, Tap to retry.");
            //alertDialog.setIcon(android.R.drawable.ic_dialog_info);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    finish();
                }
            });
            alertDialog.show();
            return;
        }*/
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Toast.makeText(this, "touch", Toast.LENGTH_SHORT).show();
        return false;
    }

    public void AlrtDialogNOMSg() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        //   alertDialog.setTitle("");
        alertDialog.setMessage("     No messages are available");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void ScreenAdjustSize(String addsize) {

        float webviewsizeHome = 0.7f;
        float webviewsizeRefresh = 0.7f;
        float thirdpartySize = 0.3f;
        if (addsize.equals("0")) {
            webviewsizeHome = 1.0f;
            webviewsizeRefresh = 1.0f;
            thirdpartySize = 0f;

        } else if (addsize.equals("1")) {
            webviewsizeHome = 1.0f;
            webviewsizeRefresh = 1.0f;
            thirdpartySize = 0f;

        } else if (addsize.equals("5")) {
            webviewsizeHome = 0.95f;
            webviewsizeRefresh = 0.95f;
            thirdpartySize = 0.05f;

        } else if (addsize.equals("10")) {
            webviewsizeHome = 0.9f;
            webviewsizeRefresh = 0.9f;
            thirdpartySize = 0.1f;

        } else if (addsize.equals("20")) {
            webviewsizeHome = 0.8f;
            webviewsizeRefresh = 0.8f;
            thirdpartySize = 0.2f;

        } else if (addsize.equals("30")) {
            webviewsizeHome = 0.7f;
            webviewsizeRefresh = 0.7f;
            thirdpartySize = 0.3f;

        } else if (addsize.equals("40")) {
            webviewsizeHome = 0.6f;
            webviewsizeRefresh = 0.6f;
            thirdpartySize = 0.4f;

        } else if (addsize.equals("50")) {
            webviewsizeHome = 0.5f;
            webviewsizeRefresh = 0.5f;
            thirdpartySize = 0.5f;
        } else if (addsize.equals("60")) {
            webviewsizeHome = 0.4f;
            webviewsizeRefresh = 0.4f;
            thirdpartySize = 0.6f;
        } else if (addsize.equals("70")) {
            webviewsizeHome = 0.3f;
            webviewsizeRefresh = 0.3f;
            thirdpartySize = 0.7f;
        } else if (addsize.equals("80")) {

            webviewsizeHome = 0.2f;
            webviewsizeRefresh = 0.2f;
            thirdpartySize = 0.8f;

        }
        LinearLayout.LayoutParams paramsRefresh = (LinearLayout.LayoutParams)
                webView_Refresh.getLayoutParams();
        paramsRefresh.weight = webviewsizeRefresh;
        webView_Refresh.setLayoutParams(paramsRefresh);

        LinearLayout.LayoutParams paramsHome = (LinearLayout.LayoutParams)
                webView_Home.getLayoutParams();
        paramsHome.weight = webviewsizeHome;
        webView_Home.setLayoutParams(paramsHome);
        LinearLayout.LayoutParams paramsThirdparty = (LinearLayout.LayoutParams)
                webView_third_party.getLayoutParams();
        paramsThirdparty.weight = thirdpartySize;
        webView_third_party.setLayoutParams(paramsThirdparty);
    }

    /*@SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        webView_Home.onResume();
        // ...
    }*/

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        webView_Home.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        webView_Home.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        webView_Refresh.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

   /* @Override
    public void onBackPressed() {
        if (!webView_Home.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }*/

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == 10101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callPhoneNumber();
            } else {
                Toast.makeText(MainActivity.this, "Call Permission is denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void callPhoneNumber() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("mobile_number", "")));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CALL_PHONE}, 10101);

            return;
        }
        MainActivity.this.startActivity(intent);
    }

}


