package com.seamnia.applications.app;

import android.app.Dialog;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonElement;
import com.seamnia.applications.app.BackEndAPI.RestClient;
import com.seamnia.applications.app.adpater.Clients_adapterView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ManageClients extends AppCompatActivity {
    Button add_user, BackPress;
    final Context context = this;
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefs";
    private String user_name;
    private String user_password;
    private String user_id;
    private RecyclerView recycler_view;
    List<String> swappingTextList;
    private boolean formAdduser_dialog;
    EditText ed_clientID;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_clients);
        add_user = (Button) findViewById(R.id.add_user);
        BackPress = (Button) findViewById(R.id.btnback);
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        recycler_view = (RecyclerView) findViewById(R.id.recyclerView_swapText);
        recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(layoutManager);
        init();
        add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddClientd_dialog();
            }
        });
        BackPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void init() {
        if (internetConnectionAvailable(500)) {
            getClientList();
        } else {
            Toast.makeText(context, "No Internet Connected", Toast.LENGTH_SHORT).show();
        }
    }
    private boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (Exception ignored) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }
    private void getClientList() {
        progressDialog = new ProgressDialog(ManageClients.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        user_name = prefs.getString("user_email", "");
        user_password = prefs.getString("user_password", "");
        RestClient.get().getallUserList(user_name, user_password, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    String url = jsonObject.getString("msg");
                    String webresponce = jsonObject.getString("msg");
                    if ((status.equals("1") && webresponce.equals("data available"))) {
                        JSONArray array = jsonObject.getJSONArray("data");
                        array.getJSONObject(0).getString("client_name");
                        array.getJSONObject(0).getString("client_id");
                        swappingTextList = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            swappingTextList.add(array.getJSONObject(i).getString("client_id"));
                        }
                        Clients_adapterView swap_adapter = new Clients_adapterView(ManageClients.this, swappingTextList);
                        recycler_view.setAdapter(swap_adapter);
                    }
                    Log.d("status", status + " :" + webresponce);
                    switch (status) {
                        case "0":
                            //   Toast.makeText(context, ""+webresponce, Toast.LENGTH_SHORT).show();
                            break;
                        case "1":
                        default:
                    }
                    try {
                        progressDialog.dismiss();
                    } catch (Exception ignored) {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }
            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }
    private void addClient(String clientid) {
        if (formAdduser_dialog) {
            RestClient.get().add_client(user_name, user_password, clientid, new Callback<JsonElement>() {
                @Override
                public void success(JsonElement jsonElement, Response response) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonElement.toString());
                        String status = jsonObject.getString("status");
                        String url = jsonObject.getString("msg");
                        String webresponce = jsonObject.getString("msg");
                        Log.d("status", status + " :" + webresponce);
                        switch (status) {
                            case "0":
                                Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                break;
                            case "-1":
                                Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                break;
                            case "2":
                                Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                break;
                            case "-2":
                                Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                break;
                            case "1":
                                Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                getClientList();
                            default:
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.v("ExceptionResult", e.toString());
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Log.v("ExceptionResult", error.toString());
                }
            });
            formAdduser_dialog = false;
        } else {
            prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            user_name = prefs.getString("user_email", "");
            user_password = prefs.getString("user_password", "");
            user_id = prefs.getString("client_id", "");
            RestClient.get().add_client(user_name, user_password, user_id, new Callback<JsonElement>() {
                @Override
                public void success(JsonElement jsonElement, Response response) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonElement.toString());
                        String status = jsonObject.getString("status");
                        String url = jsonObject.getString("msg");
                        String webresponce = jsonObject.getString("msg");
                        Log.d("status", status + " :" + webresponce);
                        switch (status) {
                            case "0":
                                Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                break;
                            case "-2":
                                Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                break;
                            case "1":
                                // Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                                getClientList();
                            default:
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.v("ExceptionResult", e.toString());
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Log.v("ExceptionResult", error.toString());
                }
            });
        }
    }
    public void delete_client(String Client) {
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        user_name = prefs.getString("user_email", "");
        user_password = prefs.getString("user_password", "");
        RestClient.get().delete_client(user_name, user_password, Client, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    String url = jsonObject.getString("msg");
                    String webresponce = jsonObject.getString("msg");
                    Log.d("status", status + " :" + webresponce);
                    switch (status) {
                        case "0":
                            Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                            break;
                        case "1":
                            Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                            getClientList();
                        case "-1":
                            Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                            break;
                        case "2":
                            Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                            break;

                        case "-2":
                            Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                            break;

                        case "3":
                            Toast.makeText(context, "" + webresponce, Toast.LENGTH_SHORT).show();
                            break;
                        default:
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    public void AddClientd_dialog() {
        final Dialog dialog = new Dialog(ManageClients.this);
        dialog.setContentView(R.layout.adduser_dialog);
        dialog.setTitle("Title...");
        ed_clientID = (EditText) dialog.findViewById(R.id.ed_clientID);
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageResource(R.mipmap.ic_launcher);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clientid = ed_clientID.getText().toString();

                if (!(clientid.length()==0)) {
                    formAdduser_dialog = true;
                    addClient(clientid);
                    dialog.dismiss();
                } else {
                    ed_clientID.setError("please insert client ID");
                }
            }
        });
        dialog.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
