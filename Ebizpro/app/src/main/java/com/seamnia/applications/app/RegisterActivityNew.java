package com.seamnia.applications.app;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonElement;
import com.seamnia.applications.app.BackEndAPI.RestClient;
import com.seamnia.applications.app.BackEndAPI.Utils;
import com.seamnia.applications.app.app.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegisterActivityNew extends AppCompatActivity {

    public String user_name, user_email, user_password, user_password_confirm, client_id, mobile_no, address;
    private String device_id;
    private String model_no;
    private String reg_id;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefs";
    Button btnback;
    private EditText inputName, inputEmail, inputPassword, inputPassword_confirm, inputClient_id, inputMobileno, inputAddress, ref_id;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutPassword, inputLayoutPassword_confirm, inputLayout_clientID, inputLayout_mobileno;
    ProgressDialog progressDialog;
    private String reference_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        btnback = (Button) findViewById(R.id.btnback);

        inputPassword_confirm = (EditText) findViewById(R.id.input_password_confirm);
        inputName = (EditText) findViewById(R.id.input_name);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputClient_id = (EditText) findViewById(R.id.input__clientID);
        //waqas
        //below two line are commented only for ebizpro
        if (BuildConfig.FLAVOR.equals("Seamnia")) {


        }else{
            inputClient_id.setText(getString(R.string.client_id));
            inputClient_id.setEnabled(false);
        }

        //waqas
        /*inputEmail,inputPassword,inputPassword_confirm  will be hide for
        Atfaalnama,Easylink,printspak,ebizpro,Alhuffaz and apply parsing define below

        inputMobileno@ebizpro.biz will be email

        inputMobileno will be password
          */

        inputEmail.setVisibility(View.GONE);
        inputPassword.setVisibility(View.GONE);
        inputPassword_confirm.setVisibility(View.GONE);

        inputMobileno = (EditText) findViewById(R.id.input__mobileNo);
        inputAddress = (EditText) findViewById(R.id.input_address);
        ref_id = (EditText) findViewById(R.id.input__refID);
        //waqas
        // ref_id visibility gone only for easylink and atfaalnama
        if (BuildConfig.FLAVOR.equals("atfaalnama") || BuildConfig.FLAVOR.equals("easylink")) {
            ref_id.setVisibility(View.GONE);
        }
        Button btnSignUp = (Button) findViewById(R.id.btn_signup);
        inputName.addTextChangedListener(new MyTextWatcher1(inputName));
        inputEmail.addTextChangedListener(new MyTextWatcher1(inputEmail));
        inputPassword.addTextChangedListener(new MyTextWatcher1(inputPassword));


        DeviceConfig();

        if (!(internetConnectionAvailable(500))) {
            Toast.makeText(RegisterActivityNew.this, "No internet Connected..!!", Toast.LENGTH_SHORT).show();

        }
        editor = getPreferences(MODE_PRIVATE).edit();
        prefs = getPreferences(MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        reg_id = pref.getString("regId", null);

        if (!(reg_id != null && !reg_id.isEmpty())) {
            loadPushToGCM("from main");
        }
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                submitForm();
            }
        });


        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

    }

    private boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        } catch (TimeoutException e) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }

    public void loadPushToGCM(final String msg) {
        BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId(msg);
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    public void DeviceConfig() {
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        model_no = android.os.Build.MODEL;
        String mobileType = android.os.Build.MANUFACTURER;
    }

    private void displayFirebaseRegId(String msg) {
        Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_SHORT).show();
        Log.d("Sucess", "Sucess");
    }


    private void submitForm() {


        if (!validateName()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        if (!validatePassword_Match()) {
            return;
        }

        if (!validateClientID()) {
            return;
        }

        if (!validateMobileNo()) {
            return;
        }
        if (!validateAddress()) {
            return;
        }

        if (inputEmail.getVisibility() == View.GONE) {
            inputEmail.setText(inputMobileno.getText().toString() + "@ebizpro.biz");
        }

        if (inputPassword.getVisibility() == View.GONE) {
            inputPassword.setText(inputMobileno.getText().toString());
        }

        if (inputPassword_confirm.getVisibility() == View.GONE) {
            inputPassword.setText(inputMobileno.getText().toString());
        }
        user_name = inputName.getText().toString();
        user_email = inputEmail.getText().toString();
        user_password = inputPassword.getText().toString();
        user_password_confirm = inputPassword.getText().toString();
        client_id = inputClient_id.getText().toString();
        mobile_no = inputMobileno.getText().toString();
        address = inputAddress.getText().toString();
        reference_id = ref_id.getText().toString();

        RegisterUserToWebSignup();
    }

    private void RegisterUserToWebSignup() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        RestClient.get().Registeruser(user_name, client_id, mobile_no, address, reference_id, user_email, user_password, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());

                    String status = jsonObject.getString("status");
                    String webresponce = jsonObject.getString("msg");
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    switch (status) {

                        case "0":// email is Already exists
                            //waqas
                            RegisterDeviceToWebDeviceNew();
                            break;
                        case "1":// new Device Added
                            //waqas
                            RegisterDeviceToWebDeviceNew();
                            break;
                        case "-2": // Client id not matching

                            if (webresponce.equals("Client id not matching")) {

                                ShoDialogBox(getApplicationContext(), "Client ID not Matched");
                            }

                            break;
                        case "3":// login Sucessfull
                            break;

                        default:
                            ShoDialogBox(getApplicationContext(), "Status Code(" + status + ")" + webresponce);

                    }

                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                } finally {
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    private void RegisterDeviceToWebDeviceLogin() {
        RestClient.get().LoginUpdateUser(user_email, user_password, client_id, reg_id, device_id, model_no, address, mobile_no, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());

                    String status = jsonObject.getString("status");
                    String webresponce = jsonObject.getString("msg");

                    String url_client_id = jsonObject.getJSONObject("data").getString("cliend_id");
                    String url = jsonObject.getJSONObject("data").getString("url");
                    if (url.equals("0")) {
                        url = "http://seamnia.net";
                    }
                    Log.d("client_id_", client_id + "url " + url);


                    //String loginUrl= jsonObject.getString("url");
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();


                    switch (status) {

                        case "0": //login id doesNaot Matched
                            ShoDialogBox(getApplicationContext(), "Password Not Matched");
                            break;
                        case "1"://  loggedin
                            editor.putInt("logged", 0);
                            editor.putString("client_id", url_client_id);
                            editor.putString("clientUrl", url);
                            editor.putString("user_email", user_email);
                            editor.putString("user_password", user_password);
                            editor.commit();

                            Log.d("log", jsonObject.getString("msg"));
                            Intent testPass1 = new Intent(getApplicationContext(), LoginActivity.class);
                            testPass1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            Utils.isfirstime = true;
                            testPass1.putExtra("user_email", user_email);
                            // testPass1.putExtra("client_id",client_id);

                            startActivity(testPass1);
                            finish();
                            //ShoDialogBox(getApplicationContext(), "Please register first");

                            break;
                        case "2":// login /device updated


                            if (webresponce.equals("Successfully login and  new device added")) {


                                editor.putInt("logged", 0);
                                editor.commit();
                                Log.d("log", jsonObject.getString("msg"));
                                Intent testPass2 = new Intent(getApplicationContext(), LoginActivity.class);
                                testPass2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

//                                testPass2.putExtra("user_email", user_email);
//                                testPass2.putExtra("client_id", client_id);

                                startActivity(testPass2);
                                finish();

                            } else {
                                ShoDialogBox(getApplicationContext(), "Please register first");
                            }

                            break;
                        case "-2":// login unSucessfull

                            if (webresponce.equals("Invalid Username/Password")) {
                                ShoDialogBox(getApplicationContext(), "Invalid user name please signup");

                            }
                            break;

                        default:
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    private void RegisterDeviceToWebDeviceNew() {
        progressDialog = new ProgressDialog(RegisterActivityNew.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        RestClient.get().LoginUpdateUser(user_email, user_password, client_id, reg_id, device_id, model_no, address, mobile_no, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                    String status = jsonObject.getString("status");
                    String webresponce = jsonObject.getString("msg");
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    switch (status) {
                        case "0": //login id doesNaot Matched
                            ShoDialogBox(getApplicationContext(), "Kindly restart your application");
                            break;
                        case "-1": //login id doesNaot Matched
                            ShoDialogBox(getApplicationContext(), "Status Code(" + status + ")" + webresponce);
                            break;
                        case "1":// login /device updated
                            if (webresponce.equals("Successfully login and  new device added")) {
                                String client_id = jsonObject.getJSONObject("data").getString("cliend_id");
                                String url = jsonObject.getJSONObject("data").getString("url");
                                String adsize = jsonObject.getJSONObject("data").getString("adsize");
                                String aboutTitle = jsonObject.getJSONObject("data").getString("about_title");
                                String advTitle = jsonObject.getJSONObject("data").getString("advert_title");
                                String fourTitle = jsonObject.getJSONObject("data").getString("four_title");
                                String fourUrl = jsonObject.getJSONObject("data").getString("four_url");
                                String event_weblink = jsonObject.getJSONObject("data").getString("event_weblink");
                                String mobile_number = jsonObject.getJSONObject("data").getString("mobile_number");
                                String client_address = jsonObject.getJSONObject("data").getString("client_address");
                                //waqas
                                String logout_falg = jsonObject.getJSONObject("data").getString("logout_falg");
                                if (logout_falg != null) {
                                    if (!logout_falg.equals("1"))
                                        logout_falg = "0";
                                }

                                String color = "#0c6fa6";
                                try {
                                    color = jsonObject.getJSONObject("data").getString("color");
                                } catch (Exception e) {
                                }

                                if (url.equals("0")) {
                                    url = "http://seamnia.net";
                                }
                                getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit().clear().commit();
                                editor.putInt("logged", 1);
                                editor.putString("client_id", client_id);
                                editor.putString("clientUrl", url);
                                editor.putString("user_email", user_email);
                                editor.putString("user_password", user_password);
                                editor.putString("adsize", adsize);
                                editor.putString("color", color);
                                editor.putString("about_title", aboutTitle);
                                editor.putString("advert_title", advTitle);
                                editor.putString("four_title", fourTitle);
                                editor.putString("four_url", fourUrl);
                                //waqas
                                editor.putString("logout_falg", logout_falg);
                                editor.putString("event_weblink", event_weblink);
                                editor.putString("mobile_number", mobile_number);
                                editor.putString("client_address", client_address);


                                editor.commit();
                                Log.d("log", jsonObject.getString("msg"));
                                Intent testPass1 = new Intent(getApplicationContext(), MainActivity.class);
                                testPass1.putExtra("user_email", user_email);
                                Utils.isfirstime = true;
                                // testPass1.putExtra("client_id", client_id);
                                startActivity(testPass1);
                                finish();
                            } else {
                                ShoDialogBox(getApplicationContext(), "Please register first");
                            }
                            //ShoDialogBox(getApplicationContext(), "Please register first");
                            break;
                        case "-2":// login unSucessfull
                            if (webresponce.equals("Invalid Username/Password")) {
                                ShoDialogBox(getApplicationContext(), "Invalid user name please signup");
                            }
                            break;
                        default:
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(RegisterActivityNew.this, "Rest Api Error", Toast.LENGTH_SHORT).show();

                Log.v("ExceptionResult", error.toString());
            }
        });
    }


    private boolean validateName() {
        if (inputName.getText().toString().trim().isEmpty()) {
            inputName.setError(getString(R.string.err_msg_name));
            // requestFocus(inputName);
            // Toast.makeText(this, "Please Enter your Name", Toast.LENGTH_SHORT).show();
            return false;

        }

        if (inputName.getText().toString().length() < 4) {
            inputName.setError(getString(R.string.err_msg_name_less));
            // requestFocus(inputName);
            //  Toast.makeText(this, "Name is too short", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            //  inputName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        if (inputEmail.getVisibility() == View.VISIBLE) {
            String email = inputEmail.getText().toString().trim();

            if (email.isEmpty() || !isValidEmail(email)) {
                inputEmail.setError(getString(R.string.err_msg_email));
                //requestFocus(inputEmail);
                return false;
            } else {
                //inputLayoutEmail.setErrorEnabled(false);
            }
            return true;
        } else {
            return true;
        }

    }

    private boolean validatePassword() {
        if (inputPassword.getVisibility() == View.VISIBLE) {
            if (inputPassword.getText().toString().trim().isEmpty()) {
                inputPassword.setError(getString(R.string.err_msg_password));
                // requestFocus(inputPassword);
                return false;
            }

            if (inputPassword.getText().toString().length() < 6) {
                inputPassword.setError(getString(R.string.err_msg_pass_less));
                // requestFocus(inputPassword);
                return false;
            } else {
                //inputLayoutPassword.setErrorEnabled(false);
            }

            return true;
        } else {
            return true;
        }
    }

    private boolean validatePassword_Match() {
        if (inputPassword.getVisibility() == View.VISIBLE && inputPassword_confirm.getVisibility() == View.VISIBLE) {
            if (!inputPassword.getText().toString().equals(inputPassword_confirm.getText().toString())) {
                inputPassword.setError(getString(R.string.err_msg_password_not_matched));
                // requestFocus(inputPassword);
                return false;
            }

            return true;
        } else {
            return true;
        }
    }

    private boolean validateClientID() {
        if (inputClient_id.getText().toString().trim().isEmpty()) {
            inputClient_id.setError(getString(R.string.err_msg_clientID));
            //requestFocus(inputClient_id);
            return false;
        } else {
            // inputLayout_clientID.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobileNo() {
        if (inputMobileno.getText().toString().trim().isEmpty()) {
            inputMobileno.setError(getString(R.string.err_msg_mobileNo));
            //requestFocus(inputMobileno);
            return false;
        }

        if (inputMobileno.getText().toString().length() < 10) {
            inputMobileno.setError(getString(R.string.err_msg_mobile_less));
            //requestFocus(inputMobileno);
            return false;
        } else {
            //inputLayout_mobileno.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAddress() {
        if (inputAddress.getText().toString().trim().isEmpty()) {
            inputAddress.setError("Enter Address");
            //requestFocus(inputMobileno);
            return false;
        } else
            return true;
    }


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void ShoDialogBox(final Context context, String message) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                RegisterActivityNew.this).create();
        // Setting Dialog Title
        alertDialog.setTitle("Notification");
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                NotificationManager notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notifManager.cancelAll();
            }
        });
        // Showing Alert Message
        try {
            alertDialog.show();
        } catch (Exception e) {
        }
    }
}

class MyTextWatcher1 implements TextWatcher {

    private View view;

    MyTextWatcher1(View view) {
        this.view = view;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {
        switch (view.getId()) {
            case R.id.input_name:
                // validateName();
                break;
            case R.id.input_email:
                // validateEmail();
                break;
            case R.id.input_password:
                // validatePassword();
                break;
        }
    }
}
