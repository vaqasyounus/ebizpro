package com.seamnia.applications.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;

import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonElement;
import com.pixplicity.easyprefs.library.Prefs;
import com.seamnia.applications.app.BackEndAPI.RestClient;
import com.seamnia.applications.app.app.Config;
import com.seamnia.applications.app.util.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SplashActivity extends AppCompatActivity {

    private String device_id;
    private String model_no;
    private String mobileType;
    Context context;
    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView txtRegId, txtMessage;
    String reg_id;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MobileAds.initialize(this, "ca-app-pub-2622548147388483~2621140161");
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
        txtRegId = (TextView) findViewById(R.id.txt_reg_id);
        txtMessage = (TextView) findViewById(R.id.txt_push_message);
        DeviceConfigisTablet();
        editor = getPreferences(MODE_PRIVATE).edit();
        prefs = getPreferences(MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        reg_id = pref.getString("regId", null);

        if (!(reg_id != null && !reg_id.isEmpty())) {
            loadPushToGCM("from main");
        } else {
            txtRegId.setText(reg_id);
        }

        Thread background = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                    SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                    String restoredText = prefs.getString("text", null);
                    int logged = prefs.getInt("logged", 0);
                    if (logged == 1) {
//                        Intent i = new Intent(getApplicationContext(), com.seamnia.applications.app.reminder.MainActivity.class);
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.putExtra("fromsplash", 1);

                        startActivity(i);
                        finish();

                    } else {
                        Intent i = new Intent(getApplicationContext(), AfterSplash.class);
//                        Intent i = new Intent(getApplicationContext(), com.seamnia.applications.app.reminder.MainActivity.class);
                        startActivity(i);
                        finish();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        background.start();
    }

    private void displayFirebaseRegId(String msg) {
        reg_id = pref.getString("regId", null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void loadPushToGCM(final String msg) {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId(msg);
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    private void PushRegisterToWeb(String reg_id) {
        RestClient.get().getRigisterOnServer(reg_id, device_id, mobileType, model_no, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonElement.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.v("ExceptionResult", e.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("ExceptionResult", error.toString());
            }
        });
    }

    public void DeviceConfigisTablet() {
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        model_no = android.os.Build.MODEL;
        mobileType = android.os.Build.MANUFACTURER;
    }
}



