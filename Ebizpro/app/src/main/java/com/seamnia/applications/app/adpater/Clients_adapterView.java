package com.seamnia.applications.app.adpater;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.seamnia.applications.app.ManageClients;
import com.seamnia.applications.app.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by karakorum on 3/21/2017.
 */

public class Clients_adapterView  extends RecyclerView.Adapter<Clients_adapterView.ViewHolder> {
    Context context;
    private List userNameList;
    private TextToSpeech t1;
    private int count_Space = 0;
    public Clients_adapterView(Context context, List UserNameList_) {
        userNameList = new ArrayList();
        userNameList = UserNameList_;
        this.context = context;
    }
    @Override
    public Clients_adapterView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycler_swappingtext_layout, parent, false);
        return new Clients_adapterView.ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(final Clients_adapterView.ViewHolder holder, int position) {
        try {
            holder.text1.setText(userNameList.get(position).toString());
            holder.delete_btn.setTag(position);
            holder.delete_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final int pos = (int) view.getTag();
                    logoutDialog(userNameList.get(pos).toString(), pos);
                }
            });
        } catch (Exception ignored) {
        }
    }
    @Override
    public int getItemCount() {
        return userNameList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text1;
        TextView delete_btn;
        ViewHolder(View view) {
            super(view);
            text1 = (TextView) view.findViewById(R.id.text);
            delete_btn = (TextView) view.findViewById(R.id.delete_btn);
            delete_btn.setBackgroundResource(R.mipmap.delete);
        }
    }
    public void logoutDialog(String client,final int pos) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure delete client " + client + " ?");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((ManageClients) context).delete_client( userNameList.get(pos).toString());
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
}