package com.seamnia.applications.app.adpater;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.seamnia.applications.app.MainActivity;
import com.seamnia.applications.app.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GroupUrl_adapterView extends RecyclerView.Adapter<GroupUrl_adapterView.ViewHolder> {
    Context context;

    String nameWordRow;
    List<String> Custom_item_list,listTime,listCid;
    public GroupUrl_adapterView(Context context, List<String> listUrls, List<String> listTime,List liscid) {
        Custom_item_list = new ArrayList();
         this.Custom_item_list = listUrls;
        this.listTime = listTime;
        this.context = context;
        this.listCid = liscid;
    }
    @Override
    public GroupUrl_adapterView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycler_groupview, parent, false);
        return new GroupUrl_adapterView.ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(final GroupUrl_adapterView.ViewHolder holder, final int position) {
        try {
            int mypos = position;
            holder.text1.setText(listTime.get(position).toString());
            mypos++;
//            holder.counter.setText(""+ mypos );
            holder.counter.setText(listCid.get(position).toString());
        } catch (Exception ignored) {
        }
        holder.text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String SelectedUrl = Custom_item_list.get(position).toString();
                Log.d("Click Position",Custom_item_list.get(position).toString());
                ((MainActivity)context).selectedGroupUrls(SelectedUrl);
            }
        });
    }
    @Override
    public int getItemCount() {
        return Custom_item_list.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text1;
        TextView counter;
        ViewHolder(View view) {
            super(view);
            text1 = (TextView) view.findViewById(R.id.text_wordname);
            counter= (TextView) view.findViewById(R.id.counter);
        }
    }
}