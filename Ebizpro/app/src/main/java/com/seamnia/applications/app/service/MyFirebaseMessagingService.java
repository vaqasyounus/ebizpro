package com.seamnia.applications.app.service;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.seamnia.applications.app.MainActivity;
import com.seamnia.applications.app.SplashActivity;
import com.seamnia.applications.app.app.Config;
import com.seamnia.applications.app.util.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import me.leolin.shortcutbadger.ShortcutBadger;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    //String encoded_string;
    Context context;
    SplashActivity activity;
    private NotificationUtils notificationUtils;
    public static final String MY_PREFS_NAME = "MyPrefs";
    public static final String MY_PREFS_BAGE = "MyPrefs Bage";
    String badgeCount = "";
    private int badgeCountNumber = 1;
    SharedPreferences.Editor editor;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        context = new MyFirebaseMessagingService();
        if (remoteMessage == null)
            return;
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                Log.v("Json", remoteMessage.getData().toString());
                JSONObject json = new JSONObject(remoteMessage.getData());
                Log.v("Json", json.toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.v("ExceptionJson", e.toString());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(final JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());
        Calendar calendar = Calendar.getInstance();
        String message = "";
        try {
            message = json.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        resultIntent.putExtra("message", message);
        showNotificationMessage(getApplicationContext(), "eBizpro", message, calendar.getTime().toString(), resultIntent);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//        notificationUtils.playNotificationSound();
        SharedPreferences prefs1 = getSharedPreferences(MY_PREFS_BAGE, MODE_PRIVATE);
        int count;
        count = prefs1.getInt("counter", 0);
        badgeCountNumber += count;

        try {
            boolean success = ShortcutBadger.applyCount(getApplicationContext(), badgeCountNumber);
            editor = getSharedPreferences(MY_PREFS_BAGE, MODE_PRIVATE).edit();
            editor.putString("baged", "1");
            editor.putInt("counter", badgeCountNumber);
            editor.commit();
            Log.d("StatusApp", "" + badgeCountNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!NotificationUtils.isAppIsInBackground(context)) {
            Toast.makeText(context, "Foreground", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Background", Toast.LENGTH_SHORT).show();
        }

    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    private void ShoDialog(final Context context, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
    }
}
